## Creating user
To use DeepBlue is necessary to have an account. For that, send an email to [deepblue@mpi-inf.mpg.de](mailto:deepblue@mpi-inf.mpg.de?Subject=New%20User), informing your name and affiliation. After processing, you will receive an email with an *user key*. Please, keep calm because it is a manual process.

![keep-calm-it-is-a-manual-process](http://deepblue.mpi-inf.mpg.de/imgs/keep-calm-it-is-a-manual-process.png)