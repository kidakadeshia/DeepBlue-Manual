## Controlled Vocabulary
We define a controlled vocabulary by a collection of unique terms, that contain name, description, and others attributes.
The controlled vocabularies are used for describing the [experiments](02-01-experiments.md) and [annotations](02-02-annotations.md).

The Bio Source terms are imported from [ENCODE controlled vocabulary][1], 

[Cell type](http://www.ontobee.org/browser/index.php?o=CL), [Experimental Factor Ontology](http://www.ontobee.org/browser/index.php?o=EFO), and [Uber anatomy ontology](http://www.ontobee.org/browser/index.php?o=UBERON)

*DeepBlue* has five controlled vocabularies: [genomes](02-04-genomes.md), *epigenetic mark*, *Bio Sources*, *techniques*, and *projects*.


[ENCODE controlled vocabulary][ftp://hgdownload.cse.ucsc.edu/apache/cgi-bin/encode/cv.ra] 